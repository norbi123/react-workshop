import React, {useState, useEffect, useReducer} from 'react';
import './App.css';

const Contact = ({contact}) => {
  console.log(contact.name)
  return (
    <>
      <div>{contact.name.first} {contact.name.last}</div>
      <div>{contact.gender}</div>
    </>
  )
}

const ContactList = (props) => {
  const contactsElem = props.contacts.map(contact => {
    return (<Contact contact={contact} />)
  })
  return (<>{contactsElem}</>)
}

function App() {
  const [value, setValue] = useState('')
  const [seed, setSeed] = useState('')
  const [contacts, setContacts] = useState([])
  

  useEffect(()=> {
    const controller = new AbortController()

    fetch('https://randomuser.me/api/?format=json&results=10&seed=' + seed, {
      signal: controller.signal
    })
    .then((result) => result.json())
    .then(result => {
      setContacts(result.results)
    })

    return () => {
      controller.abort();
    }
  }, [seed])

  const handleOnClick = () => {
    setSeed(value)
  }

  return (
    <>
      <input onChange={(event)=> {setValue(event.target.value)}} value={value}></input>

      <button onClick={handleOnClick}>get contacts</button>

      <ContactList contacts={contacts} />
    </>
  );
}

export default App;
