import React from 'react';

const GFTComponent = () => {
  const list = [
        <li>Piotr</li>,
        <li>Adam</li>,
        <li>Damian</li>
    ];
    return (
      <div>
        <h1>Hello world!</h1>
        <ul>
          {list}
        </ul>
      </div>
      );
    }

export default GFTComponent;
