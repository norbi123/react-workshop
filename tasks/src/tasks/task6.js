import React, { useEffect, useState } from 'react';
import axios from 'axios'

const Joke = (props) => {
    return (<>
        <div>ID: {props.joke.id}</div>
        <div>Type: {props.joke.type}</div>
        <div>Setup: {props.joke.setup}</div>
        <div>Punchline: {props.joke.punchline}</div>
        <br />
    </>)
}

const Jokes = (props) => {
    const [jokes, setJokes] = useState([])
    useEffect(() => {
        const getJokes = async () => {
            try {
                const result = await axios.get('https://official-joke-api.appspot.com/jokes/ten')
                setJokes(result.data)
            } catch (error) {
                console.log(error);
                setJokes([])
            }
        }

        getJokes()
    }, [])

    const jokesElems = jokes.map((joke) => {
        return (<Joke key={joke.id} joke={joke} />)
    })
    return (<>
        {jokesElems}
    </>)
}

export default Jokes;