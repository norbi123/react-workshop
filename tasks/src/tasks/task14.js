import React, {useState} from 'react';

const GFTComponent = () => {
  const [error, setError] = useState(false)
  const buttonStyle = {fontWeight: '900', backgroundColor: 'green'}

  const changeStyle = () => {
    setError(current => !current)
  }

  const dynamicClassName = `bold ${error ? 'button--red' : 'button--yellow'}`
  return (
      <>
       <button className='button--red bold'>External classes</button>
       <br />
       <button onClick={changeStyle}>Change style</button>
       <button className={dynamicClassName}>Dynamic style</button>
       <br />
       <button style={buttonStyle}>Inline style</button>
      </>
    );    
}

export default GFTComponent