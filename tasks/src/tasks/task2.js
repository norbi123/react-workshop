import React from 'react';

const GFTComponent = () => {
      const people = [ 
        {id: 0, name: 'Piotr'},
        {id: 1, name: 'Ada'},
        {id: 2, name: 'Damina'}]; 
        const list = people.map((person) => {
          return (
            <li key={person.id}>{person.name}</li>
          )
        })
      return ( 
         <ul> {list} </ul>
      );
 }

export default GFTComponent;
