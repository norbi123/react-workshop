import React, {useState, useEffect} from 'react';

const GFTComponent = () => {
   const [counter, setCounter] = useState(10)

  useEffect(() => {
    if (counter > 0) {
      document.title = counter;
    }     
  }, [counter])

  const decrement = () => {
     setCounter(counter - 1);
  };

    return (
      <>
        <button type="button" onClick={decrement}>
          -
        </button>
      </>
    );
};

export default GFTComponent