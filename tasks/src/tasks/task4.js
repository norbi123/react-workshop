import React from 'react';
import ReactDOM from 'react-dom';

export const Component1 = () => {
    return (<div>Component 1</div>)
}

export const Component2 = () => {
      return (<div>Component 2</div>)
}

export const Component3 = () => {
      return (<div>Component 3</div>)
}

ReactDOM.render(
    <>
        <Component1  />
        <Component2  />
        <Component3  />
    </>,
  document.getElementById('root')
);