import React, {useState} from 'react';

const Counter = ({decrement, increment, counter}) => {
      return (
        <>
          <button type="button" onClick={decrement}>
            -
          </button>
          {counter}
          <button type="button" onClick={increment}>
            +
          </button>
        </>
      );
  }

const GFTComponent = () => {
  const [counter, setCounter] = useState(0)
  const [clicks, setCliks] = useState(0)
  const decrement = () => {
    setCounter(counter - 1);
    setCliks(clicks + 1);
  };
  const increment = () => {
    setCounter(counter + 1);
    setCliks(clicks + 1);
  };

  return (<div>
        <Counter increment={increment}
          decrement={decrement}
          counter={counter}/>
        <p>You have clicked {clicks} times!</p>
      </div>
 );}

export default GFTComponent