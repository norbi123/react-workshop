import React, {useState} from 'react';

const GFTComponent = () => {
  const [number1, setNumber1] = useState('')
  const [number2, setNumber2] = useState('')
  const [result, setResult] = useState('')

  const onInput1Change = (event) => {
    setNumber1(Number(event.target.value))
  };

  const onInput2Change = (event) => {
    setNumber2(Number(event.target.value))
  };

  const calculate = (sign) => {
    switch (sign) {
      case '+':
        setResult(number1 + number2)
        break;
      case '-':
        setResult(number1 - number2)
        break;
      case '*':
        setResult(number1 * number2)
        break;
      case '/':
        setResult(number1 / number2)
        break;
    }
  }

    return (
      <>
        <div>
          Number1: <input onChange={ onInput1Change } value={number1} />
        </div>
        <div>
          Number2: <input onChange={ onInput2Change } value={number2} />  
        </div>
        
        <div>
          <button onClick={() => {calculate('+')}}>+</button>
          <button onClick={() => {calculate('-')}}>-</button>
          <button onClick={() => {calculate('*')}}>*</button>
          <button onClick={() => {calculate('/')}}>/</button>
        </div>

        <div>Result: {result}</div>
      </>
    );
  }

export default GFTComponent