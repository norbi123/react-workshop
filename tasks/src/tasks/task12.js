import React, {useState} from 'react';

const GFTComponent = () => {
    const [inputText, setInputText] = useState('GFT');
    const [counter, setCounter] = useState(0);
  
    const onInputChange = (event) => {
        setInputText(event.target.value)
        setCounter(counter => counter + 1)
    };

    return (
          <>
            <input onChange={onInputChange} value={inputText} />
            <div>Number of pressed keys: {counter}</div>
          </>
        );
  }


export default GFTComponent