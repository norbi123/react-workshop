//controlled
import React, {useState} from 'react';

const ControlledComponent = () => {
    const [input, setForm] = useState({
      fruit: 'coconut',
      numberOfFruit: 0
    })
    const handleInputChange = (e) => {
      setForm({
        ...input,
        [e.target.name]: e.target.value})
    }
    const handleSubmit = (e) => {
      console.log('Selected fruit: ', input);
      e.preventDefault();
    }
  return (
      <form onSubmit={handleSubmit}>
        <label>
          Pick your favorite flavor:
          <select name='fruit' value={input.fruit} onChange={ handleInputChange }>
              <option value="grapefruit">Grapefruit</option>
              <option value="lime">Lime</option>
              <option value="coconut">Coconut</option>
              <option value="mango">Mango</option>
            </select>
          </label>
          <input name='numberOfFruit' onChange={handleInputChange} value={input.numberOfFruit}></input>
          <input type="submit" value="Submit" />
        </form>
          );
      }

export default ControlledComponent

//uncontrolled
import React, { useRef } from 'react';

const GFTComponent = () => {
  const inputEl = useRef(null);
  const selectEl = useRef(null);

  const onSubmit = () => {
    console.log(inputEl.current.value)
    console.log(selectEl.current.value)
  }
  return (
      <>
        <form onSubmit={onSubmit}>
          <label>
           Pick your favorite flavor:
            <select ref={selectEl} name='fruit' defaultValue={'coconut'} >
                <option value="grapefruit">Grapefruit</option>
                <option value="lime">Lime</option>
                <option value="coconut">Coconut</option>
                <option value="mango">Mango</option>
              </select>
            </label>
          <input ref={inputEl} type="text" />
          <input type="submit" value="Submit" />
        </form>
      </>
    );    
}

export default GFTComponent