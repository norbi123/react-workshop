import React from 'react';
import PropTypes from 'prop-types';

const GFTComponent = ({text = 'is great!'}) => {
    return (<div>GFT {text}</div>)
}

GFTComponent.propTypes = {
    text: PropTypes.string
};
// old way of setting default props
// GFTComponent.defaultProps = {
//     text: 'is great'
// }

export default GFTComponent;