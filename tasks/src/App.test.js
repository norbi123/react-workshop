import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import App from './App';

test('should render label and buttons', () => {
  render(<App />);
  const incrementButton = screen.getByText(/Increment/i);
  const decrementButton = screen.getByText(/Decrement/i);
  const counterLabel = screen.getByText(/Current count/i);
  expect(incrementButton).toBeInTheDocument();
  expect(decrementButton).toBeInTheDocument();
  expect(counterLabel).toBeInTheDocument();
});

test('should increment counter', () => {
  render(<App />);
  const incrementButton = screen.getByText(/Increment/i);
  fireEvent.click(incrementButton)
  fireEvent.click(incrementButton)
  const counterLabel = screen.getByText(/Current count: 2/i);
  expect(counterLabel).toBeInTheDocument();
});

test('should decrement counter', () => {
  render(<App />);
  const decrementButton = screen.getByText(/Decrement/i);
  fireEvent.click(decrementButton)
  const counterLabel = screen.getByText(/Current count: -1/i);
  expect(counterLabel).toBeInTheDocument();
});
